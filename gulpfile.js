const gulp = require('gulp')
const zip = require('gulp-zip')
const fileindex = require('gulp-fileindex')

gulp.task('fileindex', function () {
  return gulp.src('dist/*.zip')
    .pipe(fileindex())
    .pipe(gulp.dest('./dist'))
})

gulp.task('crear_zip', () =>
  gulp.src('src/**')
    .pipe(zip('tarea5.zip'))
    .pipe(gulp.dest('dist'))
)

gulp.task('default', gulp.series('crear_zip', 'fileindex'))
